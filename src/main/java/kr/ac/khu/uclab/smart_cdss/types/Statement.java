package kr.ac.khu.uclab.smart_cdss.types;

import java.io.Serializable;

public class Statement implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private java.lang.String attribute;
  private java.lang.String comment;
  private kr.ac.khu.uclab.smart_cdss.types.Statement statement;
  
  public java.lang.String getAttribute() {
    return attribute;
  }
  public void setAttribute(java.lang.String attribute) {
    this.attribute = attribute;
  }
  public java.lang.String getComment() {
    return comment;
  }
  public void setComment(java.lang.String comment) {
    this.comment = comment;
  }
  public kr.ac.khu.uclab.smart_cdss.types.Statement getStatement() {
    return statement;
  }
  
  public void setStatement(kr.ac.khu.uclab.smart_cdss.types.Statement statement) {
    this.statement = statement;
  }
  @Override
  public String toString() {
    return "Statement [attribute=" + attribute + ", comment=" + comment
        + ", statement=" + statement + "]";
  }
}
