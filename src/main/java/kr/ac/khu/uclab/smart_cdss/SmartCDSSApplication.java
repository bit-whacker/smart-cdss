package kr.ac.khu.uclab.smart_cdss;

import jcolibri.cbraplications.StandardCBRApplication;
import jcolibri.cbrcore.CBRCase;
import jcolibri.cbrcore.CBRCaseBase;
import jcolibri.cbrcore.CBRQuery;
import jcolibri.cbrcore.Connector;
import jcolibri.exception.ExecutionException;
import jcolibri.exception.InitializingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SmartCDSSApplication implements StandardCBRApplication {
  private static Logger LOG = LogManager.getLogger(SmartCDSSApplication.class);

  private Connector connector;
  private CBRCaseBase casebase;
  
  public void configure() throws ExecutionException {
    configureConnector();
    configureCaseBase();
    preCycle();
    LOG.debug("Loaded Cases");
    for (CBRCase c : casebase.getCases()) {
      LOG.debug(c);
    }
    LOG.debug("Ready!");
  }

  private void configureConnector() throws InitializingException {
    connector = new jcolibri.connector.PlainTextConnector();
    connector.initFromXMLfile(jcolibri.util.FileIO.findFile("/plainTextConnectorConfig.xml"));
  }

  private void configureCaseBase() throws InitializingException {
    casebase = new jcolibri.casebase.LinealCaseBase();
  }

  public CBRCaseBase preCycle() throws ExecutionException {
    System.out.println("=========== before case base initialization =============== ");
    casebase.init(connector);
    System.out.println("=========== after case base initialization =============== ");
    return casebase;
  }

  public void cycle(CBRQuery query) throws ExecutionException {
    // TODO Auto-generated method stub

  }

  public void postCycle() throws ExecutionException {
    connector.close();
  }

}
