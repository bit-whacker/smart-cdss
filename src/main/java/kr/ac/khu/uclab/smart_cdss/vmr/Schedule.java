//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.11.07 at 12:02:31 PM KST 
//


package kr.ac.khu.uclab.smart_cdss.vmr;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * A schedule that specifies an event that may occur multiple times. Schedules are not used for recording when things did happen, but when they are expected or requested to occur. 
 * 
 * A schedule can be either a list of events - periods on which the event occurs, or a single event with repeating criteria, or just repeating criteria with no actual event.
 * 
 * There can only be a repeat element if there is none or one event (xpath: not(exists(f:repeat)) or count(f:event) < 2)
 * 
 * Concept borrowed from FHIR.
 * 
 * <p>Java class for Schedule complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Schedule">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:hl7-org:vmr:r2}ExtendedVmrTypeBase">
 *       &lt;sequence>
 *         &lt;element name="event" type="{urn:hl7-org:cdsdt:r2}IVL_TS" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="cycle" type="{urn:hl7-org:vmr:r2}Cycle" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Schedule", propOrder = {
    "event",
    "cycle"
})
public class Schedule
    extends ExtendedVmrTypeBase
{

    protected List<IVLTS> event;
    protected List<Cycle> cycle;

    /**
     * Gets the value of the event property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the event property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEvent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IVLTS }
     * 
     * 
     */
    public List<IVLTS> getEvent() {
        if (event == null) {
            event = new ArrayList<IVLTS>();
        }
        return this.event;
    }

    /**
     * Gets the value of the cycle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cycle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCycle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Cycle }
     * 
     * 
     */
    public List<Cycle> getCycle() {
        if (cycle == null) {
            cycle = new ArrayList<Cycle>();
        }
        return this.cycle;
    }

}
