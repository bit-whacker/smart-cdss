package kr.ac.khu.uclab.smart_cdss.representation;

import jcolibri.cbrcore.Attribute;
import jcolibri.cbrcore.CaseComponent;
import kr.ac.khu.uclab.smart_cdss.types.SmartType;

public class SmartCDSSCaseDescription implements CaseComponent {
  
  private java.lang.Integer caseId;
  private java.lang.String Value;
  private SmartType Smart;
  
  public java.lang.Integer getCaseId() {
    return caseId;
  }

  public void setCaseId(java.lang.Integer caseId0) {
    caseId = caseId0;
  }

  public java.lang.String getValue() {
    return Value;
  }

  public void setValue(java.lang.String value0) {
    Value = value0;
  }

  public SmartType getSmart() {
    return Smart;
  }

  public void setSmart(SmartType Smart0) {
    Smart = Smart0;
  }

  public Attribute getIdAttribute() {
    return (new Attribute("caseId", this.getClass()));
  }
  
  @Override
  public String toString(){
    return "SmartCDSSCaseDescription[caseId=" + caseId + ",Value=" + Value + ",Smart="+Smart+"]";
  }
}
