package kr.ac.khu.uclab.smart_cdss.representation;

import jcolibri.cbrcore.Attribute;
import jcolibri.cbrcore.CaseComponent;

public class SmartCDSSSolutionDescription implements CaseComponent {
  
  private java.lang.String Sol; 
  
  public java.lang.String getSol() {
    return Sol;
  }
  
  public void setSol(java.lang.String sol0) {
    Sol = sol0;
  }
  
  public Attribute getIdAttribute() {
    return (new Attribute("Sol", this.getClass()));
  }
  
  @Override
  public String toString(){
    return "SmartSolution[Sol=" + getSol() + "]";
  }

}
