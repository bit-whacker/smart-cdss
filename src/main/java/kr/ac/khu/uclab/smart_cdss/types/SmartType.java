package kr.ac.khu.uclab.smart_cdss.types;

import jcolibri.connector.TypeAdaptor;

import org.codehaus.jackson.type.TypeReference;

public class SmartType implements TypeAdaptor {

  private java.lang.String name;
  private java.lang.String type;
  
  
  public static SmartType createInstance(String content)
  {
    try {
      return new SmartType(content);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }
  public SmartType(){}
  public SmartType(java.lang.String content) throws Exception{
    fromString(content);
  }
  
  public java.lang.String getName() {
    return name;
  }

  public void setName(java.lang.String name) {
    this.name = name;
  }

  public java.lang.String getType() {
    return type;
  }

  public void setType(java.lang.String type) {
    this.type = type;
  }
  
  /**
   * create smartType from string contents
   * 
   * @param java.lang.String content
   */
  public void fromString(java.lang.String content) throws Exception {
    kr.ac.khu.uclab.smart_cdss.types.SmartType smartType = null;
    try {
        smartType = kr.ac.khu.uclab.smart_cdss.utils.Utilities.objectMapper.readValue(content,
                new TypeReference<kr.ac.khu.uclab.smart_cdss.types.SmartType>() {});
    } catch (Exception e) {
      System.out.println("Cannot Convert: " + content);
        e.printStackTrace();
        throw new Exception("Cannot create smart type from string contents!");
    }
    
    name = smartType.name;
    type = smartType.type;
    
    System.out.println("Done");
    System.out.println(toString());
  }
  
//  @Override
//  public boolean equals(Object obj) {
//    if (!(obj instanceof SmartType))
//      return false;
//    if (obj == this)
//      return true;
//
//    SmartType st = (SmartType) obj;
//    
//    //. case-sensitive matching
//    return (name.equals(st.getName()) && type.equals(st.getType()));
//  }
  @Override
  public boolean equals(Object obj){
    return true;
  }
  @Override
  public String toString() {
    return "{\"name\":"+getName()+",\"type\":"+getType()+"}";
  }

}
