package kr.ac.khu.uclab.smart_cdss.representation;

import jcolibri.cbrcore.Attribute;
import jcolibri.cbrcore.CaseComponent;

public class SmartCase implements CaseComponent {
  private java.lang.Integer caseId;
  private java.lang.String caseType;
  private kr.ac.khu.uclab.smart_cdss.types.EvalPerson evalPerson;
  
  
  public Attribute getIdAttribute() {
    return (new Attribute("caseId", this.getClass()));
  }


  public java.lang.Integer getCaseId() {
    return caseId;
  }


  public void setCaseId(java.lang.Integer caseId) {
    this.caseId = caseId;
  }


  public java.lang.String getCaseType() {
    return caseType;
  }


  public void setCaseType(java.lang.String caseType) {
    this.caseType = caseType;
  }


  public kr.ac.khu.uclab.smart_cdss.types.EvalPerson getEvalPerson() {
    return evalPerson;
  }


  public void setEvalPerson(kr.ac.khu.uclab.smart_cdss.types.EvalPerson evalPerson) {
    this.evalPerson = evalPerson;
  }


  @Override
  public String toString() {
    return "SmartCase [caseId=" + caseId + ", caseType=" + caseType
        + ", evalPerson=" + evalPerson + "]";
  }
}
