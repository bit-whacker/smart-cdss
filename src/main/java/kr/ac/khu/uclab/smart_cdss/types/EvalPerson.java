package kr.ac.khu.uclab.smart_cdss.types;

import org.codehaus.jackson.type.TypeReference;

import jcolibri.connector.TypeAdaptor;

public class EvalPerson implements TypeAdaptor {
  
  private java.lang.Integer age;
  private java.lang.String gender;
  private java.util.List<kr.ac.khu.uclab.smart_cdss.types.Statement> statement;
  
  public EvalPerson(){
    
  }
  public EvalPerson(String content)throws Exception{
   fromString(content); 
  }
  public void fromString(String content) throws Exception {
    kr.ac.khu.uclab.smart_cdss.types.EvalPerson ep = null;
    try {
      ep = kr.ac.khu.uclab.smart_cdss.utils.Utilities.objectMapper.readValue(content,
                new TypeReference<kr.ac.khu.uclab.smart_cdss.types.EvalPerson>() {});
    } catch (Exception e) {
      System.out.println("Cannot Convert: " + content);
        e.printStackTrace();
        throw new Exception("Cannot create smart type from string contents!");
    }
    this.age = ep.age;
    this.gender = ep.gender;
    this.statement = ep.statement;
    
    System.out.println("Done");
    System.out.println(toString());
  }


  public java.lang.Integer getAge() {
    return age;
  }


  public void setAge(java.lang.Integer age) {
    this.age = age;
  }


  public java.lang.String getGender() {
    return gender;
  }


  public void setGender(java.lang.String gender) {
    this.gender = gender;
  }


  public java.util.List<kr.ac.khu.uclab.smart_cdss.types.Statement> getStatement() {
    return statement;
  }


  public void setStatement(
      java.util.List<kr.ac.khu.uclab.smart_cdss.types.Statement> statement) {
    this.statement = statement;
  }


  @Override
  public String toString() {
    return "EvalPerson [age=" + age + ", gender=" + gender + ", statement="
        + statement + "]";
  }
  
  

}
