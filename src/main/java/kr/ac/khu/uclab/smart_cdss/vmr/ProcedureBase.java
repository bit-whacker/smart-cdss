//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.11.07 at 12:02:31 PM KST 
//


package kr.ac.khu.uclab.smart_cdss.vmr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstract base class for a procedure, which is a series of steps taken on a subject to accomplish a clinical goal. Procedures include diagnostic testing, consultations, referrals, nursing procedures, making observations, and other clinical interventions excluding substance administrations.
 * 
 * <p>Java class for ProcedureBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProcedureBase">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:hl7-org:vmr:r2}ClinicalStatement">
 *       &lt;sequence>
 *         &lt;element name="approachBodySite" type="{urn:hl7-org:vmr:r2}BodySite" minOccurs="0"/>
 *         &lt;element name="procedureCode" type="{urn:hl7-org:cdsdt:r2}CD"/>
 *         &lt;element name="procedureMethod" type="{urn:hl7-org:cdsdt:r2}CD" minOccurs="0"/>
 *         &lt;element name="targetBodySite" type="{urn:hl7-org:vmr:r2}BodySite" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcedureBase", propOrder = {
    "approachBodySite",
    "procedureCode",
    "procedureMethod",
    "targetBodySite"
})
@XmlSeeAlso({
    ProcedureEvent.class,
    ScheduledProcedure.class,
    UndeliveredProcedure.class,
    ProcedureOrder.class,
    ProcedureProposal.class
})
public abstract class ProcedureBase
    extends ClinicalStatement
{

    protected BodySite approachBodySite;
    @XmlElement(required = true)
    protected CD procedureCode;
    protected CD procedureMethod;
    protected BodySite targetBodySite;

    /**
     * Gets the value of the approachBodySite property.
     * 
     * @return
     *     possible object is
     *     {@link BodySite }
     *     
     */
    public BodySite getApproachBodySite() {
        return approachBodySite;
    }

    /**
     * Sets the value of the approachBodySite property.
     * 
     * @param value
     *     allowed object is
     *     {@link BodySite }
     *     
     */
    public void setApproachBodySite(BodySite value) {
        this.approachBodySite = value;
    }

    /**
     * Gets the value of the procedureCode property.
     * 
     * @return
     *     possible object is
     *     {@link CD }
     *     
     */
    public CD getProcedureCode() {
        return procedureCode;
    }

    /**
     * Sets the value of the procedureCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CD }
     *     
     */
    public void setProcedureCode(CD value) {
        this.procedureCode = value;
    }

    /**
     * Gets the value of the procedureMethod property.
     * 
     * @return
     *     possible object is
     *     {@link CD }
     *     
     */
    public CD getProcedureMethod() {
        return procedureMethod;
    }

    /**
     * Sets the value of the procedureMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link CD }
     *     
     */
    public void setProcedureMethod(CD value) {
        this.procedureMethod = value;
    }

    /**
     * Gets the value of the targetBodySite property.
     * 
     * @return
     *     possible object is
     *     {@link BodySite }
     *     
     */
    public BodySite getTargetBodySite() {
        return targetBodySite;
    }

    /**
     * Sets the value of the targetBodySite property.
     * 
     * @param value
     *     allowed object is
     *     {@link BodySite }
     *     
     */
    public void setTargetBodySite(BodySite value) {
        this.targetBodySite = value;
    }

}
